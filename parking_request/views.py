from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import ParkingRequest


def home(request):
    context = {
        'parking-requests': ParkingRequest.objects.all()
    }
    return render(request, 'parking_request/home.html', context)


class ParkingRequestListView(ListView):
    model = ParkingRequest
    template_name = 'parking_request/home.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'parking_requests'
    ordering = ['-date_posted']
    paginate_by = 5


class UserParkingRequestListView(ListView):
    model = ParkingRequest
    template_name = 'parking_request/user_parking_requests.html'  # <app>/<model>_<viewtype>.html
    context_object_name = 'parking_requests'
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return ParkingRequest.objects.filter(author=user).order_by('-date_posted')


class ParkingRequestDetailView(DetailView):
    model = ParkingRequest


class ParkingRequestCreateView(LoginRequiredMixin, CreateView):
    model = ParkingRequest
    fields = ['title', 'location']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class ParkingRequestUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = ParkingRequest
    fields = ['title', 'location']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        parking_request = self.get_object()
        if self.request.user == parking_request.author:
            return True
        return False


class ParkingRequestDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = ParkingRequest
    success_url = '/'

    def test_func(self):
        parking_request = self.get_object()
        if self.request.user == parking_request.author:
            return True
        return False


def about(request):
    return render(request, 'parking_request/about.html', {'title': 'About'})
