from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse


class ParkingRequest(models.Model):
    title = models.CharField(max_length=100)
    location = models.TextField()
    date_from = models.DateTimeField(default=timezone.now)
    date_to = models.DateTimeField(default=timezone.now)
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('parking-request-detail', kwargs={'pk': self.pk})
