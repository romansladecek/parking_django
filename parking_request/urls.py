from django.urls import path
from .views import (
    ParkingRequestListView,
    ParkingRequestDetailView,
    ParkingRequestCreateView,
    ParkingRequestUpdateView,
    ParkingRequestDeleteView,
    UserParkingRequestListView
)
from . import views

urlpatterns = [
    path('', ParkingRequestListView.as_view(), name='web-home'),
    path('user/<str:username>', UserParkingRequestListView.as_view(), name='user-parking-requests'),
    path('parking_request/<int:pk>/', ParkingRequestDetailView.as_view(), name='parking-request-detail'),
    path('parking_request/new/', ParkingRequestCreateView.as_view(), name='parking-request-create'),
    path('parking_request/<int:pk>/update/', ParkingRequestUpdateView.as_view(), name='parking-request-update'),
    path('parking_request/<int:pk>/delete/', ParkingRequestDeleteView.as_view(), name='parking-request-delete'),
    path('about/', views.about, name='web-about'),
]
