from django.apps import AppConfig


class ParkingRequestConfig(AppConfig):
    name = 'parking_request'
